var express = require('express');
var router = express.Router();
var orginization_dal = require('../model/orginization_dal');



// View All account
router.get('/all', function(req, res) {
    orginization_dal.getAll(function(err, result){
        if(err) {
            res.send(err);
        }
        else {
            res.render('orginization/orginizatonViewAll', { 'result':result });
        }
    });

});

// View the account for the given id
router.get('/', function(req, res){
    if(req.query.Team_Name == null) {
        res.send('Team_name is null');
    }
    else {
        orginization_dal.getById(req.query.Team_Name, function(err,result) {
            if (err) {
                res.send(err);
            }
            else {
                res.render('orginization/orginizationViewById', {'result': result});
            }
        });
    }
});

// Return the add a new company form
router.get('/add', function(req, res){
    // passing all the query parameters (req.query) to the insert function instead of each individually
    orginization_dal.getAll(function(err,result) {
        if (err) {
            res.send(err);
        }
        else {
            res.render('orginization/orginizationAdd', {'orginization': result});
        }
    });
});

// View the company for the given id
router.get('/insert', function(req, res){
    // simple validation
    if(req.query.City == null) {
        res.send('City must be provided.');
    }

    else {
        // passing all the query parameters (req.query) to the insert function instead of each individually
        orginization_dal.insert(req.query, function(err,result) {
            if (err) {
                res.send(err);
            }
            else {
                //poor practice for redirecting the user to a different page, but we will handle it differently once we start using Ajax
                res.redirect(302, '/orginization/all');
            }
        });
    }
});

router.get('/edit', function(req, res){
    if(req.query.Team_Name == null) {
        res.send('Team_name is required');
    }
    else {
        orginization_dal.edit(req.query.Team_Name, function(err, result){
            res.render('orginization/orginizationUpdate', {orginization: result[0][0]});
        });
    }

});



router.get('/update', function(req, res) {
    orginization_dal.update(req.query, function(err, result){
        res.redirect(302, '/orginization/all');
    });
});

// Delete a company for the given company_id
router.get('/delete', function(req, res){
    if(req.query.Team_Name == null) {
        res.send('Team_name is null');
    }
    else {
        orginization_dal.delete(req.query.Team_Name, function(err, result){
            if(err) {
                res.send(err);
            }
            else {
                //poor practice, but we will handle it differently once we start using Ajax
                res.redirect(302, '/orginization/all');
            }
        });
    }
});

router.get('/Ask', function(req, res){
    // passing all the query parameters (req.query) to the insert function instead of each individually
            res.render('orginization/orginizationAsk');

});

router.get('/ViewResult', function(req, res){
    if(req.query.number_req == null) {
        res.send('number_req is null');
    }
    else {
        orginization_dal.getsumppg(req.query.number_req , function(err,result) {
            if (err) {
                res.send(err);
            }
            else {
                res.render('orginization/orginizationViewResult', {'result': result});
            }
        });
    }
});
module.exports = router;