var express = require('express');
var router = express.Router();
var player_dal = require('../model/player_dal');



// View All account
router.get('/select', function(req, res) {
    player_dal.getTeams(function(err, teams){
        player_dal.getAllPlayers(function(err, player){
        if(err) {
            res.send(err);
        }
        else {
            res.render('player/playerSelect', { teams :teams, player: player });
        }
    });
    });

});


router.get('/team', function (req,res) {
    player_dal.getAll(req.query.Team_Name,function(err, result){
        if(err) {
            res.send(err);
        }
        else {
            res.render('player/playerViewByTeam', { result : result });
        }
    });

});
router.get('/add_stats', function(req, res){

        player_dal.getTeams(function(err,Teams) {
            player_dal.getById(req.query.player_id, function(err,player) {
            if (err) {
                res.send(err);
            }
            else {
                res.render('player/playerAdd_Stats', {Teams: Teams, player : player[0] });
            }
        });
        });

});

router.get('/insert_stats', function(req, res){
    // simple validation
    if(req.query.Team_name == null) {
        res.send('Team_name must be provided.');
    }
    else {
        // passing all the query parameters (req.query) to the insert function instead of each individually
        player_dal.insert_stats(req.query, function(err,result) {
            if (err) {
                res.send(err);
            }
            else {
                //poor practice for redirecting the user to a different page, but we will handle it differently once we start using Ajax
                res.redirect(302, '/player/select');
            }
        });
    }
});



router.get('/', function(req, res){
    if(req.query.player_id == null) {
        res.send('player_id is null');
    }
    else {
        player_dal.getById(req.query.player_id, function(err,result) {
            if (err) {
                res.send(err);
            }
            else {
                res.render('player/playerViewById', {'result': result});
            }
        });
    }
});

router.get('/add', function(req, res){
    // passing all the query parameters (req.query) to the insert function instead of each individually
    player_dal.getTeams(function(err,result) {
        if (err) {
            res.send(err);
        }
        else {
            res.render('player/playerAdd', {'Teams': result });
        }
    });
});

// View the company for the given id
router.get('/insert', function(req, res){
    // simple validation
    if(req.query.Team_name == null) {
        res.send('Team_name must be provided.');
    }
    else {
        // passing all the query parameters (req.query) to the insert function instead of each individually
        player_dal.insert(req.query, function(err,result) {
            if (err) {
                console.log(err);
                res.send(err);
            }
            else {
                //poor practice for redirecting the user to a different page, but we will handle it differently once we start using Ajax
                res.redirect(302, '/player/select');
            }
        });
    }
});

router.get('/delete', function(req, res){
    if(req.query.player_id == null) {
        res.send('player_id is null');
    }
    else {
        player_dal.delete(req.query.player_id, function(err, result){
            if(err) {
                res.send(err);
            }
            else {

                res.redirect(302, '/player/select');
            }
        });
    }
});

router.get('/delete_stats', function(req, res){
    if(req.query.player_id == null) {
        res.send('player_id is null');
    }
    else {
        player_dal.delete_stats(req.query, function(err, result){
            if(err) {
                res.send(err);
            }
            else {

                res.redirect(302, '/player/select');
            }
        });
    }
});


router.get('/edit', function(req, res){
    if(req.query.player_id == null) {
        res.send('A player is required');
    }
    else {
        player_dal.getById(req.query.player_id, function(err, player){
            player_dal.getTeams(function(err, teams) {

                        res.render('player/playerUpdate', { player : player[0], teams: teams});

                    });
        });
    }

});



router.get('/update', function(req, res) {
    player_dal.update(req.query, function(err, result){
        res.redirect(302, '/player/select',  { was_successful: true});
    });
});

router.get('/ViewResultPPG', function(req, res){
    if(req.query.points == null) {
        res.send('points is null');
    }
    else {
        player_dal.getResultPPG(req.query.points , function(err,result) {
            if (err) {
                res.send(err);
            }
            else {
                res.render('player/playerViewResultPPG', {'result': result});
            }
        });
    }
});

router.get('/ViewResultAPG', function(req, res){
    if(req.query.assists == null) {
        res.send('assists is null');
    }
    else {
        player_dal.getResultAPG(req.query.assists , function(err,result) {
            if (err) {
                res.send(err);
            }
            else {
                res.render('player/playerViewResultPPG', {'result': result});
            }
        });
    }
});

router.get('/ViewResultRPG', function(req, res){
    if(req.query.Rebounds == null) {
        res.send('Rebounds is null');
    }
    else {
        player_dal.getResultRPG(req.query.Rebounds , function(err,result) {
            if (err) {
                res.send(err);
            }
            else {
                res.render('player/playerViewResultPPG', {'result': result});
            }
        });
    }
});

module.exports = router;